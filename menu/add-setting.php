<?php
 
function add_setting_function() {
    
    global $wpdb;
    echo '<div class="wrap">';
    echo '<h2>Add Setting</h2>';
    echo '<div id="poststuff"><div id="post-body">';
    ?>

  <?php 

    $setting_tbl = $wpdb->prefix . "setting_tbl";

    $results = $wpdb->get_results("SELECT * FROM $setting_tbl ORDER BY id DESC");

    function objectToArray($d) {
        if (is_object($d)) {  
            $d = get_object_vars($d);
        }
  
        if (is_array($d)) {
           
            return array_map(__FUNCTION__, $d);
        }
        else {
            return $d;
        }
    }

$var1 = objectToArray($results);

    ?>
    

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<style type="text/css">
    input[type="text"],select {
        width: 450px;
    }
    .nav-tabs {
        border-bottom: 1px solid #ddd;
        background: #24282c;
    }


    .nav-tabs>li>a {
        margin-right: 2px;
        line-height: 1.42857143;
        border: 0px solid transparent;
        border-radius: 4px 4px 0 0;
        color: #fff;
    }
    .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
        color: #fff;
        border-radius: 0px;
        cursor: default;
        background-color: #009dff;
        border: 0px;
        border-bottom: 0px solid #009dff;
        border-bottom-color: transparent;
        /* line-height: 30px; */
    }
    .nav>li>a:focus, .nav>li>a:hover {
        text-decoration: none;
        background-color: #4ba1f0;
        border: 0px solid #4ba1f0;
    }
    .nav-tabs>li>a:hover {
        border-color: #4ba1f0;
    }
    .nav-tabs>li>a {
        margin-right: 2px;
        line-height: 1.42857143;
        border: 0px solid transparent;
        border-radius: 0px;
        color: #fff;
    }
</style>  
  <div class="postbox">
         
   <h3 class="hndle"><label for="title">Setting Option</label></h3><br />
    <div class="inside">
              
     <div class="">

       <form method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>">

          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#home">Post Option</a></li>
            <li><a data-toggle="tab" href="#menu1">Post Content Options</a></li>
            <li><a data-toggle="tab" href="#menu2">Posting Restrictions</a></li>
            <li><a data-toggle="tab" href="#menu3">Featured Image Options</a></li>
             <li><a data-toggle="tab" href="#menu4">Google Translation Key</a></li>
              <li><a data-toggle="tab" href="#menu5">PHP Setting</a></li>
          </ul>

          <div class="tab-content" style="border:1px solid #e5e5e5;">
            <div id="home" class="tab-pane fade in active">
        
            <!-- home -->
            <table  style="overflow-x:auto;width: 40%;">
            <br>
            <tr>          
                 <td>
                    <div class="inside">
                     <b>Check For Duplicate Posts:</b>
                    </div>
                 </td>
                 <td>
                    <div class="inside">
                     <input type="checkbox"  name="duplicate_post" <?php if($var1[0]['duplicate_post'] == 'on') echo 'checked';?>>       
                 </div>
                 </td>   
            </tr>         
    </table>

      <!-- home end -->
    </div>


    <div id="menu1" class="tab-pane fade">
      
      <!-- menu 1 -->

      <table  style="overflow-x:auto;width: 80%;">
       
       <br />

           <tr>
             <td >
                <div class="inside">
                 <b> Do Not Generate Inexistent Categories for New Posts:</b>
                </div>
             </td>
              <td >
                <div class="inside">
                 <input type="checkbox"  name="generate_inexist_categories" <?php if($var1[0]['generate_inexist_categories'] == 'on') echo 'checked';?>>       
                </div>
             </td>
          </tr>

          <tr>
             <td >
                <div class="inside">
                 <b>Keyword Replacer Tool</b>
                </div>
             </td>
              <td >
                <div class="inside">
                 <input type="text" id="read_more" style="width: 200px;" placeholder="Find Keyword" name="read_more" value="<?php echo $var1[0]['read_more'] ?>">       
                </div>
                <div class="inside">
                 <input type="text" placeholder="Replcae Link" name="user_agent" value="<?php echo base64_decode($var1[0]['user_agent']) ?>"><br />
                 Note:( Please use single quote for link (') )     
                </div>
             </td>
          </tr> 
        
          <tr>
             <td >
                <div class="inside">
                 <b>Translation Source Language (Global):</b>
                </div>
             </td>

              <td >
                <div class="inside">
                 <select id="translate_to_global_lang" name="translate_to_global_lang" style="width: 88%;">
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "disabled" ) echo 'selected' ; ?> value="disabled" selected="">Disabled</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "af" ) echo 'selected' ; ?> value="af">Afrikaans (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "sq" ) echo 'selected' ; ?> value="sq">Albanian (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "ar" ) echo 'selected' ; ?> value="ar">Arabic (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "hy" ) echo 'selected' ; ?> value="hy">Armenian (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "be" ) echo 'selected' ; ?> value="be">Belarusian (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "bg" ) echo 'selected' ; ?> value="bg">Bulgarian (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "ca" ) echo 'selected' ; ?> value="ca">Catalan (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "zh-CN" ) echo 'selected' ; ?> value="zh-CN">Chinese (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "hr" ) echo 'selected' ; ?> value="hr">Croatian (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "cs" ) echo 'selected' ; ?> value="cs">Czech (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "da" ) echo 'selected' ; ?> value="da">Danish (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "nl" ) echo 'selected' ; ?> value="nl">Dutch (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "en" ) echo 'selected' ; ?> value="en">English (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "et" ) echo 'selected' ; ?> value="et">Estonian (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "tl" ) echo 'selected' ; ?> value="tl">Filipino (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "fi" ) echo 'selected' ; ?> value="fi">Finnish (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "fr" ) echo 'selected' ; ?> value="fr">French (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "gl" ) echo 'selected' ; ?> value="gl">Galician (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "de" ) echo 'selected' ; ?> value="de">German (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "el" ) echo 'selected' ; ?> value="el">Greek (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "iw" ) echo 'selected' ; ?> value="iw">Hebrew (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "hi" ) echo 'selected' ; ?> value="hi">Hindi (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "hu" ) echo 'selected' ; ?> value="hu">Hungarian (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "id" ) echo 'selected' ; ?> value="is">Icelandic (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "id" ) echo 'selected' ; ?> value="id">Indonesian (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "ga" ) echo 'selected' ; ?> value="ga">Irish (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "it" ) echo 'selected' ; ?> value="it">Italian (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "ja" ) echo 'selected' ; ?> value="ja">Japanese (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "ko" ) echo 'selected' ; ?> value="ko">Korean (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "lv" ) echo 'selected' ; ?> value="lv">Latvian (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "lt" ) echo 'selected' ; ?> value="lt">Lithuanian (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "no" ) echo 'selected' ; ?> value="no">Norwegian (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "mk" ) echo 'selected' ; ?> value="mk">Macedonian (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "ms" ) echo 'selected' ; ?> value="ms">Malay (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "mt" ) echo 'selected' ; ?> value="mt">Maltese (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "fa" ) echo 'selected' ; ?> value="fa">Persian (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "pl" ) echo 'selected' ; ?> value="pl">Polish (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "pt" ) echo 'selected' ; ?> value="pt">Portuguese (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "ro" ) echo 'selected' ; ?> value="ro">Romanian (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "ru" ) echo 'selected' ; ?> value="ru">Russian (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "sr" ) echo 'selected' ; ?> value="sr">Serbian (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "sk" ) echo 'selected' ; ?> value="sk">Slovak (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "sl" ) echo 'selected' ; ?> value="sl">Slovenian (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "es" ) echo 'selected' ; ?> value="es">Spanish (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "sw" ) echo 'selected' ; ?> value="sw">Swahili (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "sv" ) echo 'selected' ; ?> value="sv">Swedish (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "th" ) echo 'selected' ; ?> value="th">Thai (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "tr" ) echo 'selected' ; ?> value="tr">Turkish (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "uk" ) echo 'selected' ; ?> value="uk">Ukrainian (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "vi" ) echo 'selected' ; ?> value="vi">Vietnamese (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "cy" ) echo 'selected' ; ?> value="cy">Welsh (Google Translate)</option>
                    <option <?php if ($var1[0]['translate_to_global_lang'] == "yi" ) echo 'selected' ; ?> value="yi">Yiddish (Google Translate)</option>            
                  </select>       
                </div>
             </td>
          </tr>

          <tr>
             <td >
                <div class="inside">
                 <b>Keep Original Link Source After Translation:</b>
                </div>
             </td>

              <td >
                <div class="inside">
                 <input type="checkbox" name="keep_link_source" <?php if($var1[0]['keep_link_source'] == 'on') echo 'checked';?>>       
                </div>
             </td>
          </tr> 


          <tr>
             <td >
                <div class="inside">
                 <b> Spin Text Using Word Synonyms (for automatically generated posts only):</b>
                </div>
             </td>

              <td >
                <div class="inside">
                 <select id="spin_text" name="spin_text" style="width: 88%;">
                        <option <?php if ($var1[0]['spin_text'] == "disabled" ) echo 'selected' ; ?> value="disabled" selected="">Disabled</option>                       
                        <option <?php if ($var1[0]['spin_text'] == "wikisynonyms" ) echo 'selected' ; ?> value="wikisynonyms">WikiSynonyms </option>                        
                 </select> 
                </div>
             </td>
          </tr> 


          <tr>
             <td >
                <div class="inside">
                 <b>Do Not Spin Title:</b>
                </div>
             </td>

              <td >
                <div class="inside">
                 <input type="checkbox" name="no_spin_title" <?php if($var1[0]['no_spin_title'] == 'on') echo 'checked';?>>       
                </div>
             </td>
          </tr> 

     </table>
      <!-- menu 1 -->
    </div>


    <div id="menu2" class="tab-pane fade">   
        
      <!-- menu 2 -->
<table  style="overflow-x:auto;width: 80%;">
        
        <br />
         <tr>
             <td >
                <div class="inside">
                 <b>Minimum Title Word Count:</b>
                </div>
             </td>
              <td >
                <div class="inside">
                 <input type="number" id="min_word_title" style="width: 300px;" step="1" placeholder="Input the minimum word count for the title" min="0" name="min_word_title" value="<?php echo $var1[0]['min_word_title'];?>">     
                </div>
             </td>
          </tr> 

          <tr>
             <td >
                <div class="inside">
                 <b>Maximum Title Word Count:</b>
                </div>
             </td>
              <td >
                <div class="inside">
                 <input type="number" id="max_word_title" style="width: 300px;" step="1" placeholder="Input the maximum word count for the title" min="0" name="max_word_title" value="<?php echo $var1[0]['max_word_title'];?>">     
                </div>
             </td>
          </tr> 

          <tr>
             <td >
                <div class="inside">
                 <b>Minimum Content Word Count:</b>
                </div>
             </td>
              <td >
                <div class="inside">
                 <input type="number" id="min_word_content" style="width: 300px;" step="1" placeholder="Input the maximum word count for the content" min="0" name="min_word_content" value="<?php echo $var1[0]['min_word_content'];?>">     
                </div>
             </td>
          </tr> 


          <tr>
             <td >
                <div class="inside">
                 <b>Maximum Content Word Count:</b>
                </div>
             </td>
              <td >
                <div class="inside">
                 <input type="number" id="max_word_content" style="width: 300px;" step="1" placeholder="Input the maximum word count for the content" min="0" name="max_word_content" value="<?php echo $var1[0]['max_word_content'];?>">     
                </div>
             </td>
          </tr>


          <tr>
             <td >
                <div class="inside">
                 <b>Banned Words List:</b>
                </div>
             </td>
              <td >
                <div class="inside">
                 <textarea rows="4" name="banned_words" style="width: 300px;" placeholder="Do not generate posts that contain at least one of these words. Note: Please Use a comma to separate the words(e.g. words1,words2,words3)"><?php echo $var1[0]['banned_words'];?></textarea>     
                </div>
             </td>
          </tr>


          <tr>
             <td >
                <div class="inside">
                 <b>Required Words List:</b>
                </div>
             </td>
              <td >
                <div class="inside">
                 <textarea rows="4" name="required_words" style="width: 300px;" placeholder="Do not generate posts unless they contain at least one of these words. Note: Please Use a comma to separate the words(e.g. words1,words2,words3)"><?php echo $var1[0]['required_words'];?></textarea>     
                </div>
             </td>
          </tr> 

       
          <tr>
             <td >
                <div class="inside">
                 <b>Skip Posts That Do Not Have Images:</b>
                </div>
             </td>

              <td >
                <div class="inside">
                 <input type="checkbox" name="skip_no_img" <?php if($var1[0]['skip_no_img'] == 'on') echo 'checked';?>>       
                </div>
             </td>
          </tr>

         
        </table>
      <!-- menu 2 -->
    </div>

    <div id="menu3" class="tab-pane fade">
   
      <table  style="overflow-x:auto;width: 70%;">
      
              <!-- menu 3 -->
              <br />


          <tr>
             <td >
                <div class="inside">
                 <b>Do Not Copy Featured Image Locally:</b>
                </div>
             </td>

              <td >
                <div class="inside">
                 <input type="checkbox" name="no_local_image" <?php if($var1[0]['no_local_image'] == 'on') echo 'checked';?>>       
                </div>
             </td>
          </tr>

            <tr>
             <td >
                <div class="inside">
                 <b>Featured Image Resize Width:</b>
                </div>
             </td>
              <td >
                <div class="inside">
                 <input type="number" min="1" step="1" style="width: 300px;" name="resize_width" value="<?php echo $var1[0]['resize_width'];?>" placeholder="Please insert the desired width for featured images">  
                </div>
             </td>
          </tr> 


          <tr>
             <td >
                <div class="inside">
                 <b>Featured Image Resize Height:</b>
                </div>
             </td>
              <td >
                <div class="inside">
                 <input type="number" min="1" step="1" style="width: 300px;" name="resize_height" value="<?php echo $var1[0]['resize_height'];?>" placeholder="Please insert the desired height for featured images">  
                </div>
             </td>
          </tr> 

    </table>
          <!-- menu 3 -->

    </div>


        <div id="menu4" class="tab-pane fade">
   
      <table  style="overflow-x:auto;width: 70%;">      
              <!-- menu 4 -->
              <br />
         <tr>
             <td >
                <div class="inside">
                 <b>Google Translation Key</b>
                </div>
             </td>
              <td >
                <div class="inside">
                 <input type="text" id="google_key" style="width: 300px;" placeholder="Google Translation Key" name="proxy_url" value="<?php echo $var1[0]['proxy_url'] ?>">       
                </div>
             </td>
          </tr> 
    </table>
          <!-- menu 4 -->
    </div>


     <div id="menu5" class="tab-pane fade">
   
      <table  style="overflow-x:auto;width: 70%;">
      
              <!-- menu 4 -->
              <br />

         <tr>
             <td>
                <div class="inside">
                 <b>PHP Settings</b>
                </div>
             </td>
              <td>
                <div class="inside"><strong>
                    Latest PHP Version(Above 7.0) <br />
                    This should be enable "file_get_contents" on server  <br />
                    This should be enable "curl_exec" on server
                    </strong>
       
                </div>
             </td>
          </tr>            


    </table>
          <!-- menu 4 -->

    </div>





  </div>
</div>

     <div><p class="submit"><input type="submit" name="btnSubmit" id="btnSubmit" class="button button-primary" value="Save Settings"></p></div>
    </form>

          
        </div></div>





  <!--   <div class="postbox">
        <h3 class="hndle"><label for="title">Status</label></h3>
        <div class="inside"> -->
            <?php
            include_once( 'setting_rule_class.php' ); //For rendering the license List Table
            $license_list = new WPLM_List_Setting();
            if (isset($_REQUEST['action'])) { 
                if (isset($_REQUEST['action'])) { 
                    $license_list->delete_licenses(strip_tags($_REQUEST['id']));
                }
            }
        
            $license_list->prepare_setting_list();
          
            ?>
    
 </div></div>
 </div>

    <?php
   
}

