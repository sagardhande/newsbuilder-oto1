<?php

class WPLM_RSS_FEED extends WP_License_Mgr_List_Table {
    
    function __construct(){
        global $status, $page,$wpdb;
                
        //Set parent defaults
        parent::__construct( array(
            'singular'  => 'item',     //singular name of the listed records
            'plural'    => 'items',    //plural name of the listed records
            'ajax'      => false        //does this table support ajax?
        ) );
        
    }


 function prepare_items_list() {
        


if (isset($_POST['save_rss_feed']) || isset($_POST['feed_source_url'])) { 


    global $wpdb;
    global $current_user;
    extract($_POST);

  

    $article_source_table = $wpdb->prefix . "article_source_tbl";
    $lic_key_tbl = $wpdb->prefix . "lic_key_tbl";
    $setting_tbl = $wpdb->prefix . "setting_tbl";
    
    if($_POST['feed_source_url'])
    {
        $feed_url = $_POST['feed_source_url'];
    }
    else{
        $feed_url = $_POST['default_feed_source_url']; 
    }

  
    $flag = 0;

    if(isset($_POST['postid']))
    {
        if($_POST['post_action'] == 'run_this_rule_now')
        {
            $flag = 1;
            $sql = "UPDATE $article_source_table SET source_name = '$feed_url', schedule_post = '$post_schedule', no_post ='$no_post',post_status = '$post_status', post_sel_type = '$post_sel_type',date_created = CURRENT_TIMESTAMP WHERE id = '".$_POST['postid']."'";
            $wpdb->query($sql);
            $insertid = $_POST['postid'];
        }
        elseif($_POST['post_action'] == 'delete_this_rule')
        {          
            $sql = "DELETE FROM $article_source_table WHERE id = '".$_POST['postid']."'";
            $wpdb->query($sql);
            $sql = "DELETE FROM $wpdb->posts WHERE source_post_id = '".$_POST['postid']."'";
            $wpdb->query($sql); 
        }
        elseif($_POST['post_action'] == 'duplicate_this_rule')
        {   
            $flag = 3;       
            $sql = "INSERT INTO $article_source_table (source_name, schedule_post, no_post,post_status, post_sel_type,date_created,source_type,source_category) VALUES ('$feed_url', '$post_schedule', '$no_post', '$post_status','$post_sel_type', CURRENT_TIMESTAMP,'RSS','$news_rules_list_category')";
            $wpdb->query($sql);
            $insertid = $wpdb->insert_id;
        }
        elseif($_POST['post_action'] == 'permanently_delete_all_posts')
        {          
            $sql = "DELETE FROM $wpdb->posts WHERE source_post_id = '".$_POST['postid']."'";
            $wpdb->query($sql); 
        }
        elseif($_POST['post_action'] == 'move_all_posts_to_trash')
        {          
            $sql = "UPDATE $wpdb->posts SET post_status = 'trash' WHERE source_post_id = '".$_POST['postid']."'";
            $wpdb->query($sql);
        }
    }
    else{ 

    $flag = 2;    
   
    $sql = "INSERT INTO $article_source_table (source_name, schedule_post, no_post,post_status, post_sel_type,date_created,source_type,source_category) VALUES ('$feed_url', '$post_schedule', '$no_post', '$post_status','$post_sel_type', CURRENT_TIMESTAMP,'RSS','$news_rules_list_category')";
    $wpdb->query($sql);
    $insertid = $wpdb->insert_id;
    }

     /************************** Create the category if not there ***********************/


    $result_setting = $wpdb->get_results("SELECT * FROM $setting_tbl ORDER BY id DESC");
    $lang_trans = $result_setting[0]->translate_to_global_lang; 
    $spin__text = $result_setting[0]->spin_text; 
    $dublicate_post = $result_setting[0]->duplicate_post; 

    $term = term_exists( $_POST['news_rules_list_category'], 'category' );
    
    if(count($term) > 0)
     {
       $wpdocs_cat_id = $term['term_id'];
     }
     else{
    $wpdocs_cat = array('cat_name' => $_POST['news_rules_list_category'], 'category_description' => $_POST['news_rules_list_category'] , 'category_nicename' => $_POST['news_rules_list_category'], 'category_parent' => '');
    $wpdocs_cat_id = wp_insert_category($wpdocs_cat);
    }

    
    $content = file_get_contents($feed_url);
    $x = new SimpleXmlElement($content);


   if(count($x->channel->item) && ($flag==1 || $flag==2 || $flag==3))
    {
      for($i=0;$i<count($x->channel->item);$i++)
        {
  
        $post__title = $x->channel->item[$i]->title;  
        $post__title = str_replace("'", "", $post__title); 

        $post__content = $x->channel->item[$i]->description;
        $articles__url = $x->channel->item[$i]->link;
        

        require_once (dirname(__FILE__) . "/readability.php");
        $url = $articles__url;
        if (!preg_match('!^https?://!i', $url)) $url = 'http://'.$url;

        $html = file_get_contents($url);
        $page_content = new Readability($html, $url);
        $page_content->init();
        $post__content = $page_content->articleContent->innerHTML;


         /***********spin code start*********************/
   
        if($spin__text =='wikisynonyms')
        {
         require_once(dirname(__FILE__) . "/text-spinner.php");
         $phpTextSpinner = new PhpTextSpinner();    
         $spinContent = $phpTextSpinner->spinContent($post__content);
         $translated = $phpTextSpinner->runTextSpinner($spinContent);
         $post__content = $translated;
        }
        else
        {        
         $post__content = $page_content->articleContent->innerHTML;
        }
        
        /***********spin code end *********************/

        

         if($result_setting[0]->read_more !="" && $result_setting[0]->user_agent !="")
         {
            $find__word  = $result_setting[0]->read_more;
            $replace__string = base64_decode($result_setting[0]->user_agent);
            $post__content = str_replace($find__word,$replace__string ,$post__content);
         }

        $flag_badwords = false;
        $flag_requirwords = false;

        $search_for_content = $post__title.' '.$obj->articles[$i]->description;
  
        $banned_words = explode(',', $result_setting[0]->banned_words);
        $required_words = explode(',', $result_setting[0]->required_words);

        foreach($banned_words as $badwords)
          {
         if(stristr($search_for_content,$badwords))
          {
           $flag_badwords = true;  
          }     
        }

       foreach($required_words as $requirwords)
        {
         if(stristr($search_for_content,$requirwords))
          {
           $flag_requirwords = true;  
          }      
        }

        $post_already = $wpdb->get_results("SELECT * FROM $wpdb->posts where post_title ='".ltrim($post__title)."'");
    
       
        preg_match_all('/<img.+src=[\'"](?P<src>.+?)[\'"].*>/i', $post__content, $image);
        $articles__thumbnail = $image['src'][0];

       
        $articles__thumbnail = $image['src'][0];

       if(($result_setting[0]->skip_no_img =="on"))
        {
          if($articles__thumbnail !="")
          {

       $term = term_exists( $_POST['news_rules_list_category'], 'category' );
    
        if(count($term) > 0)
         {
           $wpdocs_cat_id = $term['term_id'];
         }
         else{
        $wpdocs_cat = array('cat_name' => $_POST['news_rules_list_category'], 'category_description' => $_POST['news_rules_list_category'] , 'category_nicename' => $_POST['news_rules_list_category'], 'category_parent' => '');
        $wpdocs_cat_id = wp_insert_category($wpdocs_cat);
        }



       if($flag_badwords == false || ($result_setting[0]->banned_words == "" ))
         { 
          if($flag_requirwords == true || ($result_setting[0]->required_words == "" )) 
            {
               if((count($post_already) == 0)  || ($dublicate_post != "on") )
              {
         
          $my_post = array(
          'post_title'    => ltrim($post__title),
          'post_content'  => $post__content,
          'post_status'   => $_POST['post_status'],
          'post_excerpt'   => $articles__url,
          'post_author'   => $current_user->ID,         
          'post_parent' => 0,
          'post_category' => array( $wpdocs_cat_id ),
          'post_type'   => $_POST['post_sel_type']
        
        );              
        $postid = wp_insert_post($my_post);

        $sql = "UPDATE $wpdb->posts SET source_post_id = ".$insertid." WHERE ID = '".$postid."'";
        $wpdb->query($sql);


        $image_url        = $articles__thumbnail; // Define the image URL here
        $image_name       = 'wp-header-logo.png';
        $upload_dir       = wp_upload_dir(); // Set upload folder
        $image_data       = file_get_contents($image_url); // Get image data
        $unique_file_name = wp_unique_filename( $upload_dir['path'], $image_name ); // Generate unique name
        $filename         = basename( $unique_file_name ); // Create image file name

        // Check folder permission and define file location
        if( wp_mkdir_p( $upload_dir['path'] ) ) {
            $file = $upload_dir['path'] . '/' . $filename;
        } else {
            $file = $upload_dir['basedir'] . '/' . $filename;
        }

        // Create the image  file on the server
        file_put_contents( $file, $image_data );

        // Check image file type
        $wp_filetype = wp_check_filetype( $filename, null );

        // Set attachment data
        $attachment = array(
            'post_mime_type' => $wp_filetype['type'],
            'post_title'     => sanitize_file_name( $filename ),
            'post_content'   => '',
            'post_status'    => 'inherit'
        );

        // Create the attachment
        $attach_id = wp_insert_attachment( $attachment, $file, $postid );

        // Include image.php
        require_once(ABSPATH . 'wp-admin/includes/image.php');

        // Define attachment metadata
        $attach_data = wp_generate_attachment_metadata( $attach_id, $file );

        // Assign metadata to attachment
        wp_update_attachment_metadata( $attach_id, $attach_data );

        // And finally assign featured image to post
        set_post_thumbnail( $postid, $attach_id );


            } // dublicate post
          } // requierd words
        } //bad words


        } // if thumbnail is blank
     } 

     else
     {

        $dublicate_post = $result_setting[0]->duplicate_post; 

        if($articles__thumbnail =="")
        {
            $articles__thumbnail = "https://dummyimage.com/600x400/b6c9d4/fff.png&text=No+Image+Available.!";
        }
        else
        {
            $articles__thumbnail = $image['src'][0];
        }

             $term = term_exists( $_POST['news_rules_list_category'], 'category' );
    
        if(count($term) > 0)
         {
           $wpdocs_cat_id = $term['term_id'];
         }
         else{
        $wpdocs_cat = array('cat_name' => $_POST['news_rules_list_category'], 'category_description' => $_POST['news_rules_list_category'] , 'category_nicename' => $_POST['news_rules_list_category'], 'category_parent' => '');
        $wpdocs_cat_id = wp_insert_category($wpdocs_cat);
        }

        if($flag_badwords == false || ($result_setting[0]->banned_words == "" ))
         { 
            if($flag_requirwords == true || ($result_setting[0]->required_words == "" )) 
            {
               if((count($post_already) == 0)  || ($dublicate_post != "on") )
              {
         
          $my_post = array(
          'post_title'    => ltrim($post__title),
          'post_content'  => $post__content,
          'post_status'   => $_POST['post_status'],
          'post_excerpt'   => $articles__url,
          'post_author'   => $current_user->ID,         
          'post_parent' => 0,
          'post_category' => array( $wpdocs_cat_id ),
          'post_type'   => $_POST['post_sel_type']
        
        );              
        $postid = wp_insert_post($my_post);

        $sql = "UPDATE $wpdb->posts SET source_post_id = ".$insertid." WHERE ID = '".$postid."'";
        $wpdb->query($sql);


        $image_url        = $articles__thumbnail; // Define the image URL here
        $image_name       = 'wp-header-logo.png';
        $upload_dir       = wp_upload_dir(); // Set upload folder
        $image_data       = file_get_contents($image_url); // Get image data
        $unique_file_name = wp_unique_filename( $upload_dir['path'], $image_name ); // Generate unique name
        $filename         = basename( $unique_file_name ); // Create image file name

        // Check folder permission and define file location
        if( wp_mkdir_p( $upload_dir['path'] ) ) {
            $file = $upload_dir['path'] . '/' . $filename;
        } else {
            $file = $upload_dir['basedir'] . '/' . $filename;
        }

        // Create the image  file on the server
        file_put_contents( $file, $image_data );

        // Check image file type
        $wp_filetype = wp_check_filetype( $filename, null );

        // Set attachment data
        $attachment = array(
            'post_mime_type' => $wp_filetype['type'],
            'post_title'     => sanitize_file_name( $filename ),
            'post_content'   => '',
            'post_status'    => 'inherit'
        );

        // Create the attachment
        $attach_id = wp_insert_attachment( $attachment, $file, $postid );

        // Include image.php
        require_once(ABSPATH . 'wp-admin/includes/image.php');

        // Define attachment metadata
        $attach_data = wp_generate_attachment_metadata( $attach_id, $file );

        // Assign metadata to attachment
        wp_update_attachment_metadata( $attach_id, $attach_data );

        // And finally assign featured image to post
        set_post_thumbnail( $postid, $attach_id );


          } // dublicate post
        } // reqierd words
      } // bad words

     }
/***********************end code Featured Image to Post*****************************/

        if($i==$_POST['no_post'])
            break;
        
    } 
   }          
 
     echo '<script type="text/javascript">location.reload(true);</script>';

       
        }
            
       
    }
}